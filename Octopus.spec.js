/*eslint-env node,mocha*/
require('coffee-script/register');

import {expect} from 'chai';

import {default as Octopus, __RewireAPI__ as OctopusModule} from './Octopus';

describe('Octopus', () => {
	let octopus;
	before(() => {
		OctopusModule.__set__('blame', () => {
			return 'rewired blaming';
		});
	});
	after(() => {
		OctopusModule.__ResetDependency__('blame');
	});
	it('has to be a function', function() {
		expect(Octopus).to.be.a('function');
	});
	it('has to be able to create an instance', function() {
		octopus = new Octopus();
		expect(octopus).to.be.instanceof(Octopus);
	});
	it('has to say it\'s an octopus', function() {
		expect(octopus.say('hi')).to.eql("I'm an octopus: hi");
	});
	it('has to use the rewired blame method', function(){
		this.skip();  // rewire is not working with coffee
		expect(octopus.blame()).to.eql("rewired blaming");
	});

});
