# Sample boilerplate for ES6+CoffeeScript projects #

This project holds the basic skeleton for a mixed ES6+CoffeeScript
project, transpiled with Babel and support for mocha-based testing and
istanbul-based coverage reporting.

Also, support for the
[rewire babel plugin](https://npmjs.org/package/babel-plugin-rewire)
is configured.  See [TODO](#TODO) because it's not available for
CoffeeScript-extended classes.

## Setting up ##

To get it ready to run, please install the required stuff:

	% npm install

## Running tests  ##

	% npm run cover

This command will run the tests and write coverage report into
`./coverage`.  If you only want to run tests, please run

	% npm test
	
instead.

## TODO ##

Add real CoffeeScript support.
