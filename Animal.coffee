class Animal
	constructor: (@name) ->

	say: (text) ->
		"I'm just an animal: #{text}"

	blame: (text) ->
		text

module.exports = Animal

