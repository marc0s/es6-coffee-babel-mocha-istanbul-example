let Animal = require('./lib/Animal');

export default class Octopus extends Animal {
	say(text) {
		return `I'm an octopus: ${text}`;
	}

	tell(text) {
		return `Telling you ${text}`;
	}
}
