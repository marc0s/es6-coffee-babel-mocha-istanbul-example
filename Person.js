var _say = function (what) {
	return what;
};

export default class Person {
	constructor(name) {
		this.name = name;
	}

	greet(who) {
		return `Hi, ${who}`;
	}

	say(what) {
		return _say(what);
	}
}
