/*eslint-env mocha,node*/
import {expect} from 'chai';
import {default as Person, __RewireAPI__ as RewireAPI } from './Person';

describe('Person', () => {
	before(() => {
		RewireAPI.__set__('_say', () => {
			return 'I say that';
		});
	});
	it('should greet bob', function(){
		expect(new Person('alice').greet('bob')).to.eql('Hi, bob');
	});
	it('should use the rewired say', function() {
		expect(new Person('alice').say('whatever')).to.eql('I say that');
	});
});
